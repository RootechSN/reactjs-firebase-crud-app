import firebase from "firebase";

var firebaseConfig = {
    apiKey: "AIzaSyCtwjgQwKzHG-Ep9brdeLIE-wPDnKM_nSE",
    authDomain: "react-crud-4894d.firebaseapp.com",
    databaseURL: "https://react-crud-4894d-default-rtdb.firebaseio.com",
    projectId: "react-crud-4894d",
    storageBucket: "react-crud-4894d.appspot.com",
    messagingSenderId: "1016079852466",
    appId: "1:1016079852466:web:68acaa75a9d6b9af043aab"
  };
  // Initialize Firebase
 var fireDb = firebase.initializeApp(firebaseConfig);

  export default fireDb.database().ref();