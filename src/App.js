import logo from './logo.svg';
import './App.css';
import Contacts from './components/Contacts';

function App() {
  return (
    <div className="row">
      <div className = "container-fluid">
        <Contacts />
      </div>
    </div>
  );
}

export default App;
